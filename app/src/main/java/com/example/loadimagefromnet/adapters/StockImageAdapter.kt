package com.example.loadimagefromnet.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.loadimagefromnet.R
import com.example.loadimagefromnet.data.StockImage

class StockImageAdapter : RecyclerView.Adapter<StockImageAdapter.StockImageViewHolder>() {

    private var listStockImage = mutableListOf<StockImage>()
    var onBind = false

    private lateinit var onReachEndListener: OnReachEndListener
    private lateinit var onItemImageClickListener: OnItemImageClickListener

    interface OnReachEndListener{
        fun onReachEnd()
    }

    interface OnItemImageClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnReachEndListener(onReachEndListener: OnReachEndListener){
        this.onReachEndListener = onReachEndListener
    }

    fun setOnItemImageClickListener(onItemImageClickListener: OnItemImageClickListener){
        this.onItemImageClickListener = onItemImageClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockImageViewHolder {
        var view = LayoutInflater.from(parent.context).
            inflate(R.layout.item_image, parent, false)
        return StockImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: StockImageViewHolder, position: Int) {

        var stockImage = listStockImage.get(position)
        holder.stockImageView.setImageBitmap(stockImage.url)
        onBind = true
//        checkReachEnd(position)
        onBind = false

        holder.itemView.setOnClickListener {
            if (onItemImageClickListener != null) {
                onItemImageClickListener.onItemClick(position)
            }
        }
    }

    fun checkReachEnd(position: Int){
        if (itemCount >= 20 && position > itemCount -4 && onReachEndListener != null){
            onReachEndListener.onReachEnd()
        }
    }

    override fun getItemCount(): Int {
        return listStockImage.size
    }

    fun update(listImages: MutableList<StockImage>){
//        if (!onBind)
        listStockImage = listImages
        notifyDataSetChanged()
    }

    class StockImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val stockImageView : ImageView = itemView.findViewById(R.id.imageViewStock)

    }

}