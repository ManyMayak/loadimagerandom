package com.example.loadimagefromnet

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.loadimagefromnet.adapters.StockImageAdapter
import com.example.loadimagefromnet.data.StockImage
import com.example.loadimagefromnet.utils.Network.Companion.buildStocksUrl
import com.example.loadimagefromnet.utils.Network.Companion.buildUrl

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerViewStockImage: RecyclerView
    private lateinit var adapterStockImage: StockImageAdapter
    private lateinit var listStockImage: MutableList<StockImage>
    private var loading = true
    private var pastVisiblesItems:Int = 0
    private var visibleItemCount:Int = 0
    private var totalItemCount:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerViewStockImage = findViewById(R.id.recyclerViewImages)
        adapterStockImage = StockImageAdapter()
        recyclerViewStockImage.layoutManager = GridLayoutManager(this, getColumnCount())
        recyclerViewStockImage.adapter = adapterStockImage

        listStockImage = mutableListOf()

        adapterStockImage.setOnItemImageClickListener(object : StockImageAdapter.OnItemImageClickListener{
            override fun onItemClick(position: Int) {
               var stockImage = listStockImage.get(position)
               var intent = Intent(applicationContext, DetailActivity::class.java)
               intent.putExtra("URL", stockImage.urlImage)
                startActivity(intent)
            }

        })

//        adapterStockImage.setOnReachEndListener(object : OnReachEndListener{
//            override fun onReachEnd() {
//                fillList()
//            }
//        })

//        recyclerViewStockImage.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if(dy>0){
//                    visibleItemCount = (recyclerViewStockImage.layoutManager as GridLayoutManager).
//                        childCount
//                    totalItemCount = (recyclerViewStockImage.layoutManager as GridLayoutManager).
//                    itemCount
//                    pastVisiblesItems = (recyclerViewStockImage.layoutManager as GridLayoutManager).
//                    findFirstVisibleItemPosition()
//
//                    if (loading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount){
//                            loading = false
//                            fillList()
//                            loading = true
//                        }
//                    }
//                }
//            }
//        })

//        val request = DownloadManager.Request(Uri.parse(file.
//        val query = DownloadManager.Query()
//        val cursor = Cursor.FIELD_TYPE_BLOB
//        cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
        fillList()
//        DownloadManager.STATUS_SUCCESSFUL
//        buildFlagsUrl(43);
    }

    private fun getColumnCount(): Int{
        val displayMetr = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetr)
        var width = displayMetr.widthPixels / displayMetr.density
        return if (width.toInt() / 185 > 2) width.toInt() / 185 else 2
    }

    private fun fillList(){
        var oldSize = listStockImage.size
        while (listStockImage.size < oldSize + 20){
            val randomInteger = (1000000..9999999).random()
            var bitmap = buildStocksUrl(randomInteger)
            var url = buildUrl (randomInteger)
            if (bitmap != null){
                listStockImage.add(StockImage(bitmap, url))
            } else {
                continue
            }
        }
        adapterStockImage.update(listStockImage)
//        adapterStockImage.notifyDataSetChanged()

    }
}