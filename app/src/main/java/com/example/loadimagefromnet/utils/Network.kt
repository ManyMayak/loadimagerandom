package com.example.loadimagefromnet.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import java.io.InputStream
import java.lang.Exception
import java.lang.String
import java.net.URL

class Network {

    companion object{

        val IMAGE_PATH = "https://images.pexels.com/photos/%d/pexels-photo-%d.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"

        fun buildStocksUrl(numberID: Int): Bitmap? {
            var url = buildUrl(numberID)
            return DownloadImage().execute(url).get()
        }

        fun buildUrl(numberID: Int): kotlin.String {
            var url = String.format(IMAGE_PATH, numberID, numberID)
            return url
        }

        fun loadImage(url: kotlin.String):Bitmap{
            return DownloadImage().execute(url).get()
        }

    }

    private class DownloadImage : AsyncTask<kotlin.String, Void, Bitmap>() {

        override fun doInBackground(vararg params: kotlin.String?): Bitmap? {
            var urlDisplay = params[0]
            var mIcon : Bitmap? = null
            try {
                var inp : InputStream = URL(urlDisplay).openStream()
                mIcon = BitmapFactory.decodeStream(inp)
            }catch (e: Exception) {
                e.printStackTrace()
            }

            return mIcon
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
        }
    }
}