package com.example.loadimagefromnet

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.widget.ImageView
import android.widget.TextView
import com.example.loadimagefromnet.data.StockImage
import com.example.loadimagefromnet.utils.Network.Companion.loadImage

class DetailActivity : AppCompatActivity() {

    private lateinit var imageViewStockImage: ImageView
    private lateinit var textViewURL: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        imageViewStockImage = findViewById(R.id.imageViewStockDetail)
        textViewURL = findViewById(R.id.textViewURLImage)

        var intent = intent
        if (intent != null && intent.hasExtra("URL")){
            var url = intent.getStringExtra("URL")
//            var imageBytes = Base64.decode(url, 0)
//            var image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            imageViewStockImage.setImageBitmap(loadImage(url))
            textViewURL.text = url
        }
    }
}