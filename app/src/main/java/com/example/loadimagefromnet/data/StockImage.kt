package com.example.loadimagefromnet.data

import android.graphics.Bitmap

class StockImage(val url: Bitmap, val urlImage: String) {

    var fUrl: Bitmap = url
    var fUrlImage: String = urlImage

}